from libs.core import create_app

app = create_app('sharedemos_media_server')

if __name__ == '__main__':
    app.run()
