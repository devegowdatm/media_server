import os

from flask import Flask
from flask_restful import Api

from api import configure_api


def create_app(package_name, proj_env='development'):
    """Create app ,load configuration and initialise Api."""
    app = Flask(package_name)

    load_config(app, proj_env)

    initialize_extensions(app)
    return app


def load_config(app, proj_env='development'):
    """Load congigurations"""
    app.config.from_object('config.default')

    config_files = {
        'development': 'config/development.cfg',
        'stage': 'config/stage.cfg',
        'qa': 'config/qa.cfg'
    }

    proj_env = os.environ.get('PROJ_ENV', proj_env)

    if proj_env in config_files:
        app.config.from_pyfile(config_files[proj_env])

        for _key in ["SECRET_KEY"]:
            if os.environ.get(_key):
                app.config[_key] = os.environ[_key]

        app.config["RACKSPACE_CDN_ENABLED"] = get_env_value(
            app, 'RACKSPACE_CDN_ENABLED'
        )


def get_env_value(app, env_key):
    """
    Get the environment key's value.

    Evaluate it to True by checking its value.
    """
    return_value = False
    if app.config[env_key]:
        # If the value is from environment, then evaluate it as a string.
        if isinstance(app.config[env_key], str):
            return_value = app.config[env_key] == "True"
        else:
            return_value = app.config[env_key]
    return return_value


def initialize_extensions(app):
    """Initialise Api"""
    api_manager = Api(app)
    configure_api(api_manager)
