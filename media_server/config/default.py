import os

APP_ROOT_DIR = os.path.dirname(os.path.dirname(__file__))

STATIC_FOLDER = os.path.abspath(os.path.join(APP_ROOT_DIR, "static"))

MEDIA_FOLDER = os.path.join(STATIC_FOLDER, "media")
