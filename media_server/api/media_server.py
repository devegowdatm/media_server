from flask_restful import Resource


class MediaServer(Resource):
    """Media Server API."""
    def get(self, id=None):
        return {
            'frame_work': {'name': 'flask', 'version': '1.1.1'},
            'api': {'name': 'Flask-RESTful', 'version': '0.3.7'},
            'language': 'python'
        }, 200

    def post(self):
        return "CREATED", 201

    def put(self, id):
        return "UPDATED", 200

    def patch(self, id):
        return "UPDATED", 200

    def delete(self, id):
        return "DELETED", 200
