from .media_server import MediaServer


def configure_api(api_manager):
    api_manager.add_resource(
        MediaServer,
        '/api/media-server', '/api/media-server/',
        '/api/media-server/<int:id>'
    )
